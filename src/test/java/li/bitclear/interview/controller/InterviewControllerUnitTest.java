package li.bitclear.interview.controller;

import li.bitclear.interview.model.Author;
import li.bitclear.interview.model.Book;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static li.bitclear.interview.utils.UnitTestUtils.*;

/**
 * @author Piotr Skrobich
 * @created 28/10/2022
 */
@SpringBootTest
public class InterviewControllerUnitTest {

    @Autowired
    private InterviewController interviewController;

    @Test
    public void organizeNullUnitTest() {
        ResponseEntity<Map<Author, ? extends Collection<Book>>> responseEntity = interviewController.organize(null);

        Assertions.assertNull(responseEntity.getBody());
        Assertions.assertEquals(HttpStatus.NO_CONTENT, responseEntity.getStatusCode());
    }

    @Test
    public void organizeEmptyListUnitTest() {
        ResponseEntity<Map<Author, ? extends Collection<Book>>> responseEntity = interviewController.organize(new ArrayList<>());

        Assertions.assertNull(responseEntity.getBody());
        Assertions.assertEquals(HttpStatus.NO_CONTENT, responseEntity.getStatusCode());
    }

    @Test
    public void organizeSingleAuthorNoDuplicateListUnitTest() {
        List<Book> books = new ArrayList<>();
        Author author = Author.builder().firstName(FIRST_NAME_1).lastName(LAST_NAME_1).build();
        books.add(Book.builder().title(TITLE_1).author(author).build());
        books.add(Book.builder().title(TITLE_2).author(author).build());
        books.add(Book.builder().title(TITLE_3).author(author).build());

        ResponseEntity<Map<Author, ? extends Collection<Book>>> responseEntity = interviewController.organize(books);

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertNotNull(responseEntity.getBody());
        Assertions.assertEquals(1, responseEntity.getBody().size());
        Assertions.assertEquals(3, responseEntity.getBody().get(author).size());
    }

    @Test
    public void organizeThreeAuthorsDuplicatesListUnitTest() {
        List<Book> books = new ArrayList<>();
        Author author1 = Author.builder().firstName(FIRST_NAME_1).lastName(LAST_NAME_1).build();
        books.add(Book.builder().title(TITLE_1).author(author1).build());
        books.add(Book.builder().title(TITLE_1).author(author1).build());
        books.add(Book.builder().title(TITLE_2).author(author1).build());
        books.add(Book.builder().title(TITLE_2).author(author1).build());
        books.add(Book.builder().title(TITLE_3).author(author1).build());
        books.add(Book.builder().title(TITLE_3).author(author1).build());
        Author author2 = Author.builder().firstName(FIRST_NAME_2).build();
        books.add(Book.builder().title(TITLE_1).author(author2).build());
        books.add(Book.builder().title(TITLE_1).author(author2).build());
        books.add(Book.builder().title(TITLE_2).author(author2).build());
        books.add(Book.builder().title(TITLE_2).author(author2).build());
        Author author3 = Author.builder().lastName(LAST_NAME_3).build();
        books.add(Book.builder().title(TITLE_1).author(author3).build());
        books.add(Book.builder().title(TITLE_1).author(author3).build());

        ResponseEntity<Map<Author, ? extends Collection<Book>>> responseEntity = interviewController.organize(books);

        Assertions.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        Assertions.assertNotNull(responseEntity.getBody());
        Assertions.assertEquals(3, responseEntity.getBody().size());
        Assertions.assertEquals(3, responseEntity.getBody().get(author1).size());
        Assertions.assertEquals(2, responseEntity.getBody().get(author2).size());
        Assertions.assertEquals(1, responseEntity.getBody().get(author3).size());
    }
}
