package li.bitclear.interview.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static li.bitclear.interview.utils.UnitTestUtils.*;

/**
 * @author Piotr Skrobich
 * @created 28/10/2022
 */
public class AuthorUnitTest {


    @Test
    public void authorUnitTests() {
        Author author1 = Author.builder().firstName(FIRST_NAME_1).lastName(LAST_NAME_1).build();
        Assertions.assertEquals(FIRST_NAME_1, author1.getFirstName());
        Assertions.assertEquals(LAST_NAME_1, author1.getLastName());

        Author author2 = Author.builder().firstName(FIRST_NAME_2).build();
        Assertions.assertEquals(FIRST_NAME_2, author2.getFirstName());
        Assertions.assertNull(author2.getLastName());

        Author author3 = Author.builder().lastName(LAST_NAME_2).build();
        Assertions.assertNull(author3.getFirstName());
        Assertions.assertEquals(LAST_NAME_2, author3.getLastName());

        Author author4 = Author.builder().build();
        Assertions.assertNull(author4.getFirstName());
        Assertions.assertNull(author4.getLastName());
    }

    @Test
    public void authorEqualsUnitTests() {
        Author author1 = Author.builder().firstName(FIRST_NAME_1).lastName(LAST_NAME_1).build();
        Author author2 = Author.builder().firstName(FIRST_NAME_2).build();
        Author author3 = Author.builder().firstName(FIRST_NAME_1).lastName(LAST_NAME_1).build();

        boolean areEquals = author1.equals(author2);
        Assertions.assertNotEquals(author1.hashCode(), author2.hashCode());
        Assertions.assertFalse(areEquals);

        areEquals = author2.equals(author3);
        Assertions.assertNotEquals(author2.hashCode(), author3.hashCode());
        Assertions.assertFalse(areEquals);

        areEquals = author1.equals(author3);
        Assertions.assertEquals(author1.hashCode(), author3.hashCode());
        Assertions.assertTrue(areEquals);
    }
}
