package li.bitclear.interview.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static li.bitclear.interview.utils.UnitTestUtils.*;
import static li.bitclear.interview.utils.UnitTestUtils.LAST_NAME_2;

/**
 * @author Piotr Skrobich
 * @created 28/10/2022
 */
public class BookUnitTest {

    @Test
    public void bookUnitTests() {
        Author author1 = Author.builder().firstName(FIRST_NAME_1).lastName(LAST_NAME_1).build();

        Book book1 = Book.builder().title(TITLE_1).author(author1).build();
        Assertions.assertEquals(TITLE_1, book1.getTitle());
        Assertions.assertNotNull(book1.getAuthor());
        Assertions.assertEquals(author1, book1.getAuthor());

        Book book2 = Book.builder().author(author1).build();
        Assertions.assertNull( book2.getTitle());
        Assertions.assertNotNull(book2.getAuthor());
        Assertions.assertEquals(author1, book2.getAuthor());

        Book book3 = Book.builder().title(TITLE_2).build();
        Assertions.assertEquals(TITLE_2, book3.getTitle());
        Assertions.assertNull(book3.getAuthor());

        Book book4 = Book.builder().build();
        Assertions.assertNull(book4.getAuthor());
        Assertions.assertNull(book4.getTitle());
    }

    @Test
    public void authorEqualsUnitTests() {
        Author author1 = Author.builder().firstName(FIRST_NAME_1).lastName(LAST_NAME_1).build();

        Book book1 = Book.builder().title(TITLE_1).author(author1).build();
        Book book2 = Book.builder().title(TITLE_2).author(author1).build();
        Book book3 = Book.builder().title(TITLE_1).author(author1).build();

        boolean areEquals = book1.equals(book2);
        Assertions.assertNotEquals(book1.hashCode(), book2.hashCode());
        Assertions.assertFalse(areEquals);

        areEquals = book2.equals(book3);
        Assertions.assertNotEquals(book2.hashCode(), book3.hashCode());
        Assertions.assertFalse(areEquals);

        areEquals = book1.equals(book3);
        Assertions.assertEquals(book1.hashCode(), book3.hashCode());
        Assertions.assertTrue(areEquals);
    }
}
