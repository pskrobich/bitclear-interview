package li.bitclear.interview.converter;

import li.bitclear.interview.model.Author;
import li.bitclear.interview.model.Book;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static li.bitclear.interview.utils.UnitTestUtils.*;

/**
 * @author Piotr Skrobich
 * @created 28/10/2022
 */
public class CollectionToMapConverterUnitTest {

    List<Book> books;

    @BeforeEach
    public void init() {
        books = new ArrayList<>();
    }

    @Test
    public void convertNullUnitTest() {
        Map<Author, ? extends Collection<Book>> authorBooksMap = CollectionToMapConverter.organize(null);

        Assertions.assertEquals(0, authorBooksMap.size());
    }

    @Test
    public void convertNullBookUnitTest() {
        books.add(null);

        Map<Author, ? extends Collection<Book>> authorBooksMap = CollectionToMapConverter.organize(books);

        Assertions.assertEquals(0, authorBooksMap.size());
    }

    @Test
    public void convertNullAuthorAndTitleTest() {
        books.add(Book.builder().build());

        Map<Author, ? extends Collection<Book>> authorBooksMap = CollectionToMapConverter.organize(books);

        Assertions.assertEquals(0, authorBooksMap.size());
    }

    @Test
    public void convertNullTitleUnitTest() {
        Author author1 = Author.builder().firstName(FIRST_NAME_1).lastName(LAST_NAME_1).build();
        books.add(Book.builder().title(TITLE_1).author(author1).build());
        books.add(Book.builder().title(TITLE_2).author(author1).build());
        books.add(Book.builder().author(author1).build());
        Author author2 = Author.builder().firstName(FIRST_NAME_2).lastName(LAST_NAME_2).build();
        books.add(Book.builder().author(author2).build());

        Map<Author, ? extends Collection<Book>> authorBooksMap = CollectionToMapConverter.organize(books);

        Assertions.assertEquals(1, authorBooksMap.size());
        Assertions.assertEquals(2, authorBooksMap.get(author1).size());
    }

    @Test
    public void convertNullAuthorUnitTest() {
        Author author = Author.builder().firstName(FIRST_NAME_1).lastName(LAST_NAME_1).build();
        books.add(Book.builder().title(TITLE_1).author(author).build());
        books.add(Book.builder().title(TITLE_2).build());

        Map<Author, ? extends Collection<Book>> authorBooksMap = CollectionToMapConverter.organize(books);

        Assertions.assertEquals(1, authorBooksMap.size());
        Assertions.assertEquals(1, authorBooksMap.get(author).size());
    }

    @Test
    public void convertSingleAuthorNoDuplicatesUnitTest() {
        Author author = Author.builder().firstName(FIRST_NAME_1).lastName(LAST_NAME_1).build();
        books.add(Book.builder().title(TITLE_1).author(author).build());
        books.add(Book.builder().title(TITLE_2).author(author).build());
        books.add(Book.builder().title(TITLE_3).author(author).build());

        Map<Author, ? extends Collection<Book>> authorBooksMap = CollectionToMapConverter.organize(books);

        Assertions.assertEquals(1, authorBooksMap.size());
        Assertions.assertEquals(3, authorBooksMap.get(author).size());
    }

    @Test
    public void convertSingleAuthorDuplicatesUnitTest() {
        Author author = Author.builder().firstName(FIRST_NAME_1).lastName(LAST_NAME_1).build();
        books.add(Book.builder().title(TITLE_1).author(author).build());
        books.add(Book.builder().title(TITLE_2).author(author).build());
        books.add(Book.builder().title(TITLE_2).author(author).build());
        books.add(Book.builder().title(TITLE_3).author(author).build());
        books.add(Book.builder().title(TITLE_1).author(author).build());
        books.add(Book.builder().title(TITLE_4).author(author).build());

        Map<Author, ? extends Collection<Book>> authorBooksMap = CollectionToMapConverter.organize(books);

        Assertions.assertEquals(1, authorBooksMap.size());
        Assertions.assertEquals(4, authorBooksMap.get(author).size());
    }

    @Test
    public void convertTwoAuthorsNoDuplicatesUnitTest() {
        Author author1 = Author.builder().firstName(FIRST_NAME_1).lastName(LAST_NAME_1).build();
        books.add(Book.builder().title(TITLE_1).author(author1).build());
        books.add(Book.builder().title(TITLE_2).author(author1).build());
        books.add(Book.builder().title(TITLE_3).author(author1).build());
        Author author2 = Author.builder().firstName(FIRST_NAME_2).build();
        books.add(Book.builder().title(TITLE_1).author(author2).build());
        books.add(Book.builder().title(TITLE_2).author(author2).build());

        Map<Author, ? extends Collection<Book>> authorBooksMap = CollectionToMapConverter.organize(books);

        Assertions.assertEquals(2, authorBooksMap.size());
        Assertions.assertEquals(3, authorBooksMap.get(author1).size());
        Assertions.assertEquals(2, authorBooksMap.get(author2).size());
    }

    @Test
    public void convertTwoAuthorsDuplicatesUnitTest() {
        Author author1 = Author.builder().lastName(LAST_NAME_1).build();
        books.add(Book.builder().title(TITLE_1).author(author1).build());
        books.add(Book.builder().title(TITLE_2).author(author1).build());
        books.add(Book.builder().title(TITLE_2).author(author1).build());
        books.add(Book.builder().title(TITLE_3).author(author1).build());
        Author author2 = Author.builder().firstName(FIRST_NAME_2).lastName(LAST_NAME_2).build();
        books.add(Book.builder().title(TITLE_1).author(author2).build());
        books.add(Book.builder().title(TITLE_1).author(author2).build());
        books.add(Book.builder().title(TITLE_2).author(author2).build());
        books.add(Book.builder().title(TITLE_2).author(author2).build());
        books.add(Book.builder().title(TITLE_3).author(author2).build());
        books.add(Book.builder().title(TITLE_4).author(author2).build());

        Map<Author, ? extends Collection<Book>> authorBooksMap = CollectionToMapConverter.organize(books);

        Assertions.assertEquals(2, authorBooksMap.size());
        Assertions.assertEquals(3, authorBooksMap.get(author1).size());
        Assertions.assertEquals(4, authorBooksMap.get(author2).size());
    }

    @Test
    public void convertThreeAuthorsNoDuplicatesUnitTest() {
        Author author1 = Author.builder().firstName(FIRST_NAME_1).lastName(LAST_NAME_1).build();
        books.add(Book.builder().title(TITLE_1).author(author1).build());
        books.add(Book.builder().title(TITLE_2).author(author1).build());
        books.add(Book.builder().title(TITLE_3).author(author1).build());
        Author author2 = Author.builder().firstName(FIRST_NAME_2).build();
        books.add(Book.builder().title(TITLE_1).author(author2).build());
        books.add(Book.builder().title(TITLE_2).author(author2).build());
        Author author3 = Author.builder().lastName(LAST_NAME_3).build();
        books.add(Book.builder().title(TITLE_1).author(author3).build());

        Map<Author, ? extends Collection<Book>> authorBooksMap = CollectionToMapConverter.organize(books);

        Assertions.assertEquals(3, authorBooksMap.size());
        Assertions.assertEquals(3, authorBooksMap.get(author1).size());
        Assertions.assertEquals(2, authorBooksMap.get(author2).size());
        Assertions.assertEquals(1, authorBooksMap.get(author3).size());

    }

    @Test
    public void convertThreeAuthorsDuplicatesUnitTest() {
        Author author1 = Author.builder().firstName(FIRST_NAME_1).lastName(LAST_NAME_1).build();
        books.add(Book.builder().title(TITLE_1).author(author1).build());
        books.add(Book.builder().title(TITLE_1).author(author1).build());
        books.add(Book.builder().title(TITLE_2).author(author1).build());
        books.add(Book.builder().title(TITLE_2).author(author1).build());
        books.add(Book.builder().title(TITLE_3).author(author1).build());
        books.add(Book.builder().title(TITLE_3).author(author1).build());
        Author author2 = Author.builder().firstName(FIRST_NAME_2).build();
        books.add(Book.builder().title(TITLE_1).author(author2).build());
        books.add(Book.builder().title(TITLE_1).author(author2).build());
        books.add(Book.builder().title(TITLE_2).author(author2).build());
        books.add(Book.builder().title(TITLE_2).author(author2).build());
        Author author3 = Author.builder().lastName(LAST_NAME_3).build();
        books.add(Book.builder().title(TITLE_1).author(author3).build());
        books.add(Book.builder().title(TITLE_1).author(author3).build());

        Map<Author, ? extends Collection<Book>> authorBooksMap = CollectionToMapConverter.organize(books);

        Assertions.assertEquals(3, authorBooksMap.size());
        Assertions.assertEquals(3, authorBooksMap.get(author1).size());
        Assertions.assertEquals(2, authorBooksMap.get(author2).size());
        Assertions.assertEquals(1, authorBooksMap.get(author3).size());

    }
}
