package li.bitclear.interview.utils;

/**
 * @author Piotr Skrobich
 * @created 28/10/2022
 */
public class UnitTestUtils {
    public static final String FIRST_NAME_1 = "First Name 1";
    public static final String FIRST_NAME_2 = "First Name 2";
    public static final String LAST_NAME_1 = "Last Name 1";
    public static final String LAST_NAME_2 = "Last Name 2";
    public static final String LAST_NAME_3 = "Last Name 3";
    public static final String TITLE_1 = "Title 1";
    public static final String TITLE_2 = "Title 2";
    public static final String TITLE_3 = "Title 3";
    public static final String TITLE_4 = "Title 4";
}
