package li.bitclear.interview.converter;

import li.bitclear.interview.model.Author;
import li.bitclear.interview.model.Book;
import org.apache.commons.collections4.CollectionUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Piotr Skrobich
 * @created 28/10/2022
 */
public class CollectionToMapConverter {

    private CollectionToMapConverter() {

    }

    public static Map<Author, ? extends Collection<Book>> organize(Collection<Book> books) {
        return CollectionUtils.emptyIfNull(books).stream()
                .filter(CollectionToMapConverter::filterBook)
                .collect(Collectors.groupingBy(
                                Book::getAuthor,
                                Collectors.mapping(
                                        book -> book, Collectors.toSet())
                        )
                );
    }

    private static boolean filterBook(Book book) {
        return book != null && book.getTitle() != null
                && filterAuthor(book.getAuthor());
    }

    private static boolean filterAuthor(Author author) {
        return author != null && (author.getFirstName() != null || author.getLastName() != null);
    }
}
