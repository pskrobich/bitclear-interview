package li.bitclear.interview.model;

import lombok.Builder;
import lombok.Getter;

/**
 * @author Piotr Skrobich
 * @created 28/10/2022
 */
@Builder
@Getter
public class Book {
    private Author author;
    private String title;

    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (!(obj instanceof Book book))
            return false;
        boolean authorEquals = (this.author == null && book.author == null)
                || this.author != null && this.author.equals(book.author);
        boolean titleEquals = (this.title == null && book.title == null)
                || this.title != null && this.title.equals(book.title);
        return authorEquals && titleEquals;
    }

    @Override
    public int hashCode() {
        int result = 9;
        if (title != null) {
            result = 452 * result + title.hashCode();
        }
        if (author != null) {
            result = 452 * result + author.hashCode();
        }
        return result;
    }
}
