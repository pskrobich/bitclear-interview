package li.bitclear.interview.model;

import lombok.Builder;
import lombok.Getter;

/**
 * @author Piotr Skrobich
 * @created 28/10/2022
 */
@Builder
@Getter
public class Author {
    private String firstName;
    private String lastName;

    @Override
    public boolean equals(Object obj) {
        if (obj == this)
            return true;
        if (!(obj instanceof Author author))
            return false;
        boolean firstNameEquals = (this.firstName == null && author.firstName == null)
                || this.firstName != null && this.firstName.equals(author.firstName);
        boolean lastNameEquals = (this.lastName == null && author.lastName == null)
                || this.lastName != null && this.lastName.equals(author.lastName);
        return firstNameEquals && lastNameEquals;
    }

    @Override
    public int hashCode() {
        int result = 9;
        if (firstName != null) {
            result = 452 * result + firstName.hashCode();
        }
        if (lastName != null) {
            result = 452 * result + lastName.hashCode();
        }
        return result;
    }
}
