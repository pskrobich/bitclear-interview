package li.bitclear.interview.controller;

import li.bitclear.interview.converter.CollectionToMapConverter;
import li.bitclear.interview.model.Author;
import li.bitclear.interview.model.Book;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Piotr Skrobich
 * @created 28/10/2022
 */
@RestController
public class InterviewController {

    @RequestMapping(value = "/organize", method = RequestMethod.POST)
    public ResponseEntity<Map<Author, ? extends Collection<Book>>> organize(@RequestBody List<Book> books) {
        Map<Author, ? extends Collection<Book>> resultMap = CollectionToMapConverter.organize(books);
        if (resultMap.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(resultMap);
    }

}
